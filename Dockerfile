FROM node:alpine as build

# Set env variable
ARG REACT_APP_API_URL

ENV REACT_APP_API_URL=$REACT_APP_API_URL


WORKDIR /opt/mrz-ui

COPY package.json .
COPY ./public /opt/mrz-ui/public
COPY ./src /opt/mrz-ui/src
COPY ./.eslintrc.js /opt/mrz-ui/
COPY ./.npmrc /opt/mrz-ui/
COPY ./.prettierrc /opt/mrz-ui/

RUN npm install

RUN npm run build

FROM nginx:alpine

EXPOSE 80

COPY --from=build /opt/mrz-ui/build /usr/share/nginx/html
