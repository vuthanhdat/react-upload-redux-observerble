##Documentation

### Quick Start 
1.  Make sure that you have Node.js v8.15.1 and npm v5 or above installed.
2.  Clone this repo using `git clone https://bitbucket.org/ubitecag/mrz-ui.git <YOUR_PROJECT_NAME>`
3.  Move to the appropriate directory: `cd <YOUR_PROJECT_NAME>`.<br />
4.  Run `npm install` in order to install dependencies.<br />
    _At this point you can run `npm start` to see the app at `http://localhost:3000`._

### Building & Deploying

1.  Run `npm run build`, which will compile all the necessary files to the
    `build` folder.
2.  Upload the contents of the `build` folder to your web server's root folder.

### Project Structure
The [`src/`](../../tree/master/src) directory contains your entire application code <br>
The [`public/`](../../tree/master/public) directory contains your HTML code

The rest of the folders and files only exist to make your life easier, and
should not need to be touched. (Except [`.env`]() file)

### Change the /get-mrz API Endpoint
Replace the value in `REACT_APP_API_URL` from `.env` file and Re-build/Restart your application

### Library in use 
`rxjs:` https://github.com/ReactiveX/rxjs<br>
`redux-observe:` https://redux-observable.js.org <br>
`material-ui:` https://material-ui.com

### Deployment on Production  
1.  Run `docker-compose up -d --build`, which will build/rebuild the docker image of SIC-UI 
2.  The current expose port for serving the UI is `3001`