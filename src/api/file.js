const apiUrl = process.env.REACT_APP_API_URL
  ? process.env.REACT_APP_API_URL
  : 'http://localhost:8080/mrz';

export const rxUpload = data => ({
  url: apiUrl,
  method: 'POST',
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  },
  body: { ...data },
});
