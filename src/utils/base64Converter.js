export const fileToBase64 = file =>
  new Promise(resolve => {
    const reader = new FileReader();
    // Convert data to base64
    reader.readAsDataURL(file);
    // Read file content on file loaded event
    reader.onload = function(event) {
      resolve(event.target.result);
    };
  });
