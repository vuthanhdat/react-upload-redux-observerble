import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';

import React from 'react';

import Grid from '@material-ui/core/Grid';
import withStyles from '@material-ui/core/styles/withStyles';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { Field, reduxForm } from 'redux-form';
import {
  uploadFileAction,
  cancelUploadAction,
} from '../redux/reducers/uploadReducer';
import MrzResult from '../components/MrzResult';
import MenuAppBar from '../components/MenuAppBar';
import UploadInput from '../components/UploadInput';
import { fileToBase64 } from '../utils/base64Converter';

class App extends React.Component {
  handleFormSubmit = async values => {
    let base64String = await fileToBase64(values.file);
    base64String = base64String.substr(base64String.indexOf(',') + 1);
    const mrzRequest = {
      fileData: base64String,
    };
    await this.props.onSubmit(mrzRequest);
  };

  render() {
    const {
      classes,
      mrzResult,
      errorMessage,
      loading,
      handleSubmit,
      formValues,
    } = this.props;
    const fileUploaded = !!formValues && !!formValues.file;
    return (
      <div className={classes.root}>
        <MenuAppBar />
        <Container
          maxWidth={false}
          className={`${classes.background} ${classes.container}`}
        >
          <form onSubmit={handleSubmit(this.handleFormSubmit)}>
            <Grid alignItems="center" justify="center" container spacing={4}>
              <Grid item lg={fileUploaded ? 6 : 12}>
                <Typography
                  variant="h5"
                  gutterBottom
                >
                  Upload Image
                </Typography>
                <Grid
                  item
                  className={`${
                    fileUploaded ? classes.paper : classes.paperLg
                  } ${classes.paperUpload}`}
                >
                  <Field customStyle={fileUploaded ? classes.paper : classes.paperLg} name="file" component={UploadInput} />
                </Grid>
              </Grid>
              {fileUploaded ? (
                <Grid item lg={6}>
                  <Typography variant="h5" gutterBottom>
                    MRZ Code
                  </Typography>
                  <Grid className={`${classes.paper} ${classes.paperNormal}`}>
                    <MrzResult
                      fileUploaded={fileUploaded}
                      loading={loading}
                      errorMessage={errorMessage}
                      mrzResult={mrzResult}
                    />
                  </Grid>
                </Grid>
              ) : (
                <div></div>
              )}
            </Grid>
          </form>
        </Container>
      </div>
    );
  }
}

const useStyles = theme => ({
  root: {
    flexGrow: 2,
  },
  background: {
    background: '#FFFFFF 0% 0% no-repeat padding-box',
  },
  container: {
    paddingTop: '307px',
  },
  paper: {
    [theme.breakpoints.down('xs')]: {
      width: 'auto',
      height: '396px',
    },
    [theme.breakpoints.down('sm')]: {
      width: '500px',
      height: '396px',
    },
    [theme.breakpoints.up('md')]: {
      width: '774px',
      height: '396px',
    },
    [theme.breakpoints.up('lg')]: {
      width: 'auto',
      height: '396px',
    },
    textAlign: 'center',
  },
  paperLg: {
    [theme.breakpoints.down('xs')]: {
      width: 'auto',
      height: '396px',
    },
    [theme.breakpoints.down('sm')]: {
      width: '500px',
      height: '396px',
    },
    [theme.breakpoints.up('md')]: {
      width: '774px',
      height: '396px',
    },
    [theme.breakpoints.up('lg')]: {
      width: 'auto',
      height: '396px',
    },
    textAlign: 'center',
  },
  paperUpload: {
    color: theme.palette.text.secondary,
    boxShadow: '1px 1px 4px #00000029',
    border: '2px dashed #707070',
    borderRadius: '2px',
    opacity: 1,
  },
  paperNormal: {
    boxShadow: '1px 1px 4px #00000029',
    borderRadius: '2px',
    opacity: 1,
  },
  menuButton: {
    marginRight: theme.spacing(92),
  },
  title: {
    flexGrow: 1,
  },
  button: {
    margin: theme.spacing(1),
    background: '#F26422',
    '&:hover': {
      backgroundColor: '#F26422',
      borderColor: '#FFFFFF',
      boxShadow: 'none',
    },
    '&:focus': {
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
    },
  },
});

const mapStateToProps = state => ({
  formValues: state.form.uploadForm.values,
  mrzResult: state.file.mrzResult,
  errorMessage: state.file.errorMessage,
  progress: state.file.progress,
  loading: state.file.loading,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      onSubmit: uploadFileAction,
      onCancel: cancelUploadAction,
    },
    dispatch,
  );

const fields = ['file'];

const validateForm = values => {
  const errors = {};
  if (!values.file) {
    errors.file = 'No file selected.';
  }
  return errors;
};

export default compose(
  withStyles(useStyles),
  reduxForm({
    form: 'uploadForm',
    validate: validateForm,
    fields,
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(App);
