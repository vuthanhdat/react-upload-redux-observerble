import { createAction, handleActions } from 'redux-actions';

export const UPLOAD_FILE = 'file/upload';
export const UPLOAD_FILE_SUCCESS = 'file/upload_success';
export const UPLOAD_FILE_FAILED = 'file/upload_failed';
export const CANCEL_UPLOAD = 'file/cancel_upload';
export const SET_UPLOAD_PROGRESS = 'file/set_upload_progress';

export const uploadFileAction = createAction(UPLOAD_FILE);
export const uploadFileSuccessAction = createAction(UPLOAD_FILE_SUCCESS);
export const uploadFileFailedAction = createAction(UPLOAD_FILE_FAILED);
export const cancelUploadAction = createAction(CANCEL_UPLOAD);
export const setUploadProgressAction = createAction(
  SET_UPLOAD_PROGRESS,
  v => v,
);

const initState = {
  mrzResult: '',
  progress: 0,
  loading: false,
  errorMessage: '',
};

const uploadReducer = handleActions(
  {
    [UPLOAD_FILE]: state => ({
      ...state,
      loading: true,
      errorMessage: '',
    }),
    [UPLOAD_FILE_SUCCESS]: (state, action) => {
      const {
        response: { response },
      } = action.payload;
      return {
        ...state,
        mrzResult: response,
        loading: false,
        errorMessage: '',
      };
    },
    [UPLOAD_FILE_FAILED]: (state, action) => ({
      ...state,
      loading: false,
      mrzResult: '',
      errorMessage: handleMrzError(action.payload),
    }),
    // eslint-disable-next-line no-unused-vars
    [CANCEL_UPLOAD]: state => ({
      ...state,
      message: '',
    }),
    [SET_UPLOAD_PROGRESS]: (state, action) => ({
      ...state,
      progress: action.payload,
    }),
  },
  initState,
);

const handleMrzError = payload => {
  if (!!payload) {
    const { response } = payload;
    return response && response.message
      ? response.message
      : ' The connection to the server could not be established. Please retry.';
  }
  return ' The connection to the server could not be established. Please retry.';
};

export default uploadReducer;
