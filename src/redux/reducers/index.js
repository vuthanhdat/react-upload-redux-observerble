import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import uploadReducer from './uploadReducer';

const rootReducer = combineReducers({
  file: uploadReducer,
  form: formReducer,
});

export default rootReducer;
