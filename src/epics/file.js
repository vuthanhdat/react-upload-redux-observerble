import { of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map, catchError, takeUntil, mergeMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';

import { rxUpload } from '../api/file';
import {
  UPLOAD_FILE,
  uploadFileSuccessAction,
  uploadFileFailedAction,
  CANCEL_UPLOAD,
} from '../redux/reducers/uploadReducer';

// TODO: progress
export const uploadFile = action$ =>
  action$.pipe(
    ofType(UPLOAD_FILE),
    mergeMap(action =>
      ajax(rxUpload(action.payload)).pipe(
        map(response => uploadFileSuccessAction({ response })),
        catchError(err => of(uploadFileFailedAction(err))),
        takeUntil(action$.ofType(CANCEL_UPLOAD)),
      ),
    ),
  );
