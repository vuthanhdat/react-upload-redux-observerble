import React from 'react';
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import textLogo from '../img/text.svg';
import errorIcon from '../img/cancel.svg';

class MrzResult extends React.Component {
  renderMrzResult = (mrz, errorMessage) => {
    if (!mrz || !mrz.length) {
      if (errorMessage) {
        return (
          <div>
            <img src={errorIcon} alt="" />
            <Typography variant="h6" color="error" component="div">
              {errorMessage}
            </Typography>
          </div>
        );
      }
      return (
        <>
          <img src={textLogo} alt="" />
          <Typography variant="body2" color="textSecondary" component="h3">
            Please click button bellow to extract MRZ code
          </Typography>
        </>
      );
    }
    if (typeof (mrz) === 'string') {
      const mrzArray = mrz.replace(/[\[\]']/g, '').split(',');
      return mrzArray.map(item => (
        <Typography variant="h6" component="div" key={item} color="textPrimary">
          {item}
        </Typography>
      ));
    }
  };

  renderPlaceHolder = (mrzResult, errorMessage) => {
    const { fileUploaded, classes } = this.props;
    const placeHolder = this.renderMrzResult(mrzResult, errorMessage);
    if (fileUploaded) {
      return (
        <div>
          <div className={classes.paddingTop90}>{placeHolder}</div>
          <div className={classes.paddingTop90}>
            <Button
              id="send"
              variant="contained"
              color="primary"
              className={classes.button}
              type="submit"
            >
              Recognize
            </Button>
          </div>
        </div>
      );
    }
  };

  render() {
    const { classes, mrzResult, errorMessage, loading } = this.props;
    const placeHolder = this.renderPlaceHolder(mrzResult.mrzData, errorMessage);

    if (loading) {
      return (
        <div className={classes.paddingTop90}>
          <CircularProgress className={classes.progress} />
          <Typography variant="h6" component="div">
            Please wait...
          </Typography>
        </div>
      );
    }
    return <>{placeHolder}</>;
  }
}

const useStyles = theme => ({
  card: {
    margin: theme.spacing(1),
  },
  content: {
    height: 380,
    margin: theme.spacing(1),
  },
  progress: {
    margin: theme.spacing(2),
    color: '#F26422',
  },
  button: {
    margin: theme.spacing(1),
    background: '#F26422',
    '&:hover': {
      backgroundColor: '#F26422',
      borderColor: '#FFFFFF',
      boxShadow: 'none',
    },
    '&:focus': {
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
    },
  },
  paddingTop90: {
    paddingTop: '90px',
  },
  paddingBottom90: {
    paddingBottom: '90px',
  },
});

export default withStyles(useStyles)(MrzResult);
