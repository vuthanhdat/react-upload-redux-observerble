import React from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import makeStyles from '@material-ui/core/styles/makeStyles';
import ubitecLogo from '../img/ubitec.svg';

const useStyle = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  background: {
    background: '#FFFFFF 0% 0% no-repeat padding-box',
  },
  margin: {
    margin: 0,
  },
  logo: {
    width: '108px',
    height: '31px',
    paddingLeft: '123px',
    opacity: 1,
  },
}));

const MenuAppBar = () => {
  const classes = useStyle();

  return (
    <div className={classes.root}>
      <AppBar
        position="absolute"
        className={`${classes.background} ${classes.margin}`}
      >
        <Toolbar>
          <img className={classes.logo} src={ubitecLogo} alt="" />
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default MenuAppBar;
