import React, { useCallback } from 'react';
import { useDropzone } from 'react-dropzone';
import Typography from '@material-ui/core/Typography';

const DragFileHere = () => (
  <div>
    <Typography variant="h6" gutterBottom>
      Drag your file here
    </Typography>
  </div>
);

const DropzoneUploader = ({
  customStyle,
  children,
  callbackInputState,
  options,
}) => {
  const onDrop = useCallback(
    acceptedFiles => {
      const file = acceptedFiles[0];
      callbackInputState(file);
    },
    [callbackInputState],
  );
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });
  return (
    <div {...getRootProps()} className={customStyle}>
      <input {...getInputProps()} {...options} multiple={false} accept="image/*" />
      {isDragActive ? <DragFileHere /> : children}
    </div>
  );
};

export default DropzoneUploader;
