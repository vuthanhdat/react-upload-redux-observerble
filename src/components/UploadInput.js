import React from 'react';
import { withStyles } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import photoImg from '../img/photo.svg'
import DropzoneUploader from './DropzoneUploader';
class UploadInput extends React.Component {

  handleImgPreviewDragAndDrop = image => {
    const {
      input: { onChange },
    } = this.props;
    if (image) {
      onChange(image);
    }
  };

  renderPreviewImg = value => {
    const { classes } = this.props;
    if (value) {
      const imgUrl = window.URL.createObjectURL(value);
      return <img src={imgUrl} className={classes.media} alt="" />;
    }
    return '';
  };

  renderUploadLabelText = () => (
    <div>
      <img src={photoImg} alt="" />
      <Typography variant="h6" gutterBottom>
        Drag and drop and image
      </Typography>
      <Typography variant="subtitle1" gutterBottom>
        Or
      </Typography>
    </div>
  );

  render() {
    const {
      input,
      name,
      error,
      meta: { touched },
      classes,
      customStyle,
      ...options
    } = this.props;
    const { value } = input;
    const preRenderedImg = this.renderPreviewImg(value);
    const uploadLabel = this.renderUploadLabelText();
    return (
        <DropzoneUploader customStyle={customStyle} value={value} options={options} callbackInputState={this.handleImgPreviewDragAndDrop}>
          <div style={{ paddingTop: '50px' }}>
            {preRenderedImg || uploadLabel}
          </div>
          <label htmlFor="contained-button-file">
            <Button
              variant="contained"
              component="span"
              className={classes.button}
            >
              Browse a file
            </Button>
          </label>
          </DropzoneUploader>
    );
  }
}

const useStyles = theme => ({
  card: {
    margin: theme.spacing(1),
  },
  media: {
    width: '369px',
    height: '231px',
    margin: theme.spacing(1),
  },
  button: {
    color: '#FFFFFF',
    background: '#F26422',
    '&:hover': {
      backgroundColor: '#F26422',
      borderColor: '#FFFFFF',
      boxShadow: 'none',
    },
    '&:focus': {
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
    },
  },
  input: {
    display: 'none',
  },
});

export default withStyles(useStyles)(UploadInput);
